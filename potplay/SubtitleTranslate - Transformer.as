/*
	real time subtitle translate for PotPlayer using localhost transformer
*/

// void OnInitialize()
// void OnFinalize()
// string GetTitle() 														-> get title for UI
// string GetVersion														-> get version for manage
// string GetDesc()															-> get detail information
// string GetLoginTitle()													-> get title for login dialog
// string GetLoginDesc()													-> get desc for login dialog
// string GetUserText()														-> get user text for login dialog
// string GetPasswordText()													-> get password text for login dialog
// string ServerLogin(string User, string Pass)								-> login
// string ServerLogout()													-> logout
//------------------------------------------------------------------------------------------------
// array<string> GetSrcLangs() 												-> get source language
// array<string> GetDstLangs() 												-> get target language
// string Translate(string Text, string &in SrcLang, string &in DstLang) 	-> do translate !!
/**
*
 * 需要搭配本地自建的后端翻译引擎使用 所以目前只支持ENtoZN
*/
string JsonParseRestult(string json)
{
    //显示拼音
    bool Lookpinyin=false;
	JsonReader Reader;
	JsonValue Root;
	string ret = "";//返回值
	
	if (Reader.parse(json, Root) && Root.isObject()) {//如果成功解析了json内容,要求是对象模式
		if(Lookpinyin){
            JsonValue pinyin = Root["info"]["pronunciation"]["translation"];//拿到翻译结果拼音
            ret=pinyin.asString()+"\n";
        }
        JsonValue translation = Root["translation"];//拿到翻译结果
		if (translation.isString()){
            ret+=translation.asString();
        }
	}

	return ret;
}
/**
* 支持的语言列表
 * 我只是更改了其他插件的代码
 * 需要搭配本地自建的后端翻译引擎使用 所以目前只支持ENtoZN
*/
array<string> LangTable = 
{
	"af",
	"sq",
	"am",
	"ar",
	"hy",
	"az",
	"eu",
	"be",
	"bn",
	"bs",
	"bg",
	"my",
	"ca",
	"ceb",
	"ny",
	"zh",
	"zh-CN",
	"zh-TW",
	"co",
	"hr",
	"cs",
	"da",
	"nl",
	"en",
	"eo",
	"et",
	"tl",
	"fi",
	"fr",
	"fy",
	"gl",
	"ka",
	"de",
	"el",
	"gu",
	"ht",
	"ha",
	"haw",
	"iw",
	"hi",
	"hmn",
	"hu",
	"is",
	"ig",
	"id",
	"ga",
	"it",
	"ja",
	"jw",
	"kn",
	"kk",
	"km",
	"ko",
	"ku",
	"ky",
	"lo",
	"la",
	"lv",
	"lt",
	"lb",
	"mk",
	"ms",
	"mg",
	"ml",
	"mt",
	"mi",
	"mr",
	"mn",
	"my",
	"ne",
	"no",
	"ps",
	"fa",
	"pl",
	"pt",
	"pa",
	"ro",
	"romanji",
	"ru",
	"sm",
	"gd",
	"sr",
	"st",
	"sn",
	"sd",
	"si",
	"sk",
	"sl",
	"so",
	"es",
	"su",
	"sw",
	"sv",
	"tg",
	"ta",
	"te",
	"th",
	"tr",
	"uk",
	"ur",
	"uz",
	"vi",
	"cy",
	"xh",
	"yi",
	"yo",
	"zu"
};

string UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";

string GetTitle()
{
	return "{$CP949=링바(구글) 번역$}{$CP950=Lingva(Google) 翻譯$}{$CP0=transformer翻译$}";
}

string GetVersion()
{
	return "1";
}

string GetDesc()
{
	return "";
}

string GetLoginTitle()
{
	return "";
}

string GetLoginDesc()
{
	return "";
}

string GetUserText()
{
	return "Server URL:";
}

string GetPasswordText()
{
	return "";
}

string server_url;

string ServerLogin(string User, string Pass)
{
	server_url = User;
	if (server_url.empty()) server_url = "http://localhost:5000";
	return "200 ok";
}

void ServerLogout()
{
	server_url = "";
}
/**
* 获取支持的语言列表 - 源语言
*/
array<string> GetSrcLangs()
{
	array<string> ret = LangTable;
	
	ret.insertAt(0, ""); // empty is auto
	return ret;
}
/**
* 获取支持的语言列表 - 目标语言
*/
array<string> GetDstLangs()
{
	array<string> ret = LangTable; 
	return ret;
}
/**
* 获取语言(将翻译语言转换成地址栏对应语言)  例如选择Chinese (Traditional)地址栏对应的是zh_HANT
*/
string GetLang(string &in lang){
    string result = lang;

    if(result.empty()){//空字符串
        result = "auto";
    } else if(result == "zh-CN"){//简体中文
        result = "zh";
    } else if(result == "zh-TW"){//繁体中文
        result = "zh_HANT";
    }


    return result;
}
string Translate(string Text, string &in SrcLang, string &in DstLang)
{
//HostOpenConsole();	// for debug
    //语言选择
    SrcLang = GetLang(SrcLang);
    DstLang = GetLang(DstLang); 

	string enc = HostUrlEncode(Text);
    string url = server_url + "/translate?text=" + enc;
    //请求翻译
	string res = HostUrlGetString(url, UserAgent);
	string ret = JsonParseRestult(res);
	if (ret.length() > 0)
	{
		SrcLang = "UTF8";
		DstLang = "UTF8";
	}

	return ret;
}
