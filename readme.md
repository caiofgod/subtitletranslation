# python version 3.7 
    pip install   PyQt5 
    pip install   transformers


模型选择

    tokenizer = AutoTokenizer.from_pretrained("Helsinki-NLP/opus-mt-en-zh")
    model = AutoModelForSeq2SeqLM.from_pretrained("Helsinki-NLP/opus-mt-en-zh")

可以登录 https://huggingface.co/models 选择合适的翻译模型 将翻译模型的名字进行替换
其中 

    Helsinki-NLP/opus-mt-en-zh

就是我选择的 英文——中文 的翻译模型


### 拖拽之后窗口可能有未响应的字样  别急,那是后台正在逐句翻译 这需要时间

### 翻译的结果将会写入到 *D:/result.txt* 中

### 您也可以通过修改  *dragEnterEvent(self, evn)* 函数来修改结果文件的位置



___
# 更新 2024.3
### 一种新的方法 
### 找到安装 potplay 播放器的文件夹
### 我这里是放在了 D:\PotPlayer 路径
### 找到 D:\PotPlayer\Extension\Subtitle\Translate 这个文件夹 这里有很多内置的翻译插件，但因为网络问题一般无法访问google 和 bing的翻译引擎
### 这里我们就可以在python端自己创建一个本地服务接口提供翻译工作
    # python version 3.7
    pip install   transformers
    pip install flask==2.2.5

### 运行SERVE目录下的 serveAPI.py 这样我们就在本地创建一个后端接口 这个接口接受GET请求 将接收到的英文转换成中文再返回
### 在transText.http中测试一下
    # curl "http://localhost:5000/translate?text=i+love+you"
    GET http://localhost:5000/translate?text=i%20love%20you 
    
### 如果得到以下内容即为成功

    http://localhost:5000/translate?text=i love you

    HTTP/1.1 200 OK
    Server: Werkzeug/2.2.3 Python/3.7.16
    Date: Wed, 20 Mar 2024 06:56:52 GMT
    Content-Type: application/json
    Content-Length: 42
    Connection: close

    {
    "translation": "我爱你"
    }

### 我们创建了一个后端接受 http://localhost:5000/translate/* 的接口 这个接口接受GET请求 将接收到的英文转换成中文再返回

### 接下来我们将 potplay 文件夹中
    SubtitleTranslate - Transformer.as
    SubtitleTranslate - Transformer.ico
### 两个文件放到之前提到的 D:\PotPlayer\Extension\Subtitle\Translate中
### 运行 erveAPI.py 重启 potplay 播放器 将potplay播放器的实时字幕翻译引擎设置成我们的 Transform翻译 即可享受实时翻译英文字幕的功能





    


